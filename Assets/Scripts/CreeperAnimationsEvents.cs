﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreeperAnimationsEvents : MonoBehaviour
{

    CreeperBehaviour creeper;
    private void Start()
    {
        creeper = GetComponentInParent<CreeperBehaviour>();
    }
    public void EndExplosion()
    {
        creeper.Explosion();
    }
}