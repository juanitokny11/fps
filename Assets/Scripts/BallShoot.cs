﻿using UnityEngine;
using System.Collections;

public class BallShoot : MonoBehaviour
{

    public Camera camera;
    public GameObject bullet_prefab;
    float bulletImpulse = 20f;


    // Update is called once per frame
    public void Shot()
    {
        GameObject thebullet = (GameObject)Instantiate(bullet_prefab, camera.transform.position + camera.transform.forward, camera.transform.rotation);
        thebullet.GetComponent<Rigidbody>().AddForce(camera.transform.forward * bulletImpulse, ForceMode.Impulse);
    }
}