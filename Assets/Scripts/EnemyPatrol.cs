﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyPatrol : MonoBehaviour
{

    public Collider[] patrolPoints;
    private int currentPoint = 0;
    // Use this for initialization
    private NavMeshAgent agent;

    IEnumerator Start()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.SetDestination(patrolPoints[currentPoint].gameObject.transform.position);
        while (true)
        {
            if (agent.isOnOffMeshLink)
            {
                yield return StartCoroutine(Parabola(agent, 0.5f, 0.5f));
                agent.CompleteOffMeshLink();
            }

            yield return null;
        }


    }

    IEnumerator Parabola(NavMeshAgent agent, float height, float duration)
    {
        OffMeshLinkData data = agent.currentOffMeshLinkData;
        Vector3 startPos = agent.transform.position;
        Vector3 endPos = data.endPos + Vector3.up * agent.baseOffset;
        float timeCounter = 0.0f;
        while (timeCounter < 1.0f)
        {
            float yOffset = height * 4.0f * (timeCounter - timeCounter * timeCounter);
            agent.transform.position = Vector3.Lerp(startPos, endPos, timeCounter) + yOffset * Vector3.up;
            timeCounter += Time.deltaTime / duration;
            yield return null;
        }
    }
    

    void OnTriggerEnter(Collider col)
    {
        currentPoint++;
        currentPoint = currentPoint % patrolPoints.Length;
        agent.SetDestination(patrolPoints[currentPoint].gameObject.transform.position);
    }
}