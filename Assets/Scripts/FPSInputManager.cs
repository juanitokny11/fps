﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSInputManager : MonoBehaviour
{

    private PlayerMovement playerController;
    private float sensitivity = 3.0f;
    private LookRotation lookRotation;
    private MouseCursor mouseCursor;
    private Laser laser;
    private BallShoot ballShoot;


    void Start()
    {
        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();
        lookRotation = playerController.GetComponent<LookRotation>();
        laser = playerController.GetComponent<Laser>();
        ballShoot = playerController.GetComponent<BallShoot>();

        mouseCursor = new MouseCursor();
        mouseCursor.HideCursor();
    }
    void Update()
    {
        //El movimiento del player
        Vector2 inputAxis = Vector2.zero;
        inputAxis.x = Input.GetAxis("Horizontal");
        inputAxis.y = Input.GetAxis("Vertical");
        playerController.SetAxis(inputAxis);
        //El salto del player
        if (Input.GetButton("Jump")) playerController.StartJump();

        //Rotación de la cámara
        Vector2 mouseAxis = Vector2.zero;
        mouseAxis.x = Input.GetAxis("Mouse X") * sensitivity;
        mouseAxis.y = Input.GetAxis("Mouse Y") * sensitivity;
        //Debug.Log("Mouse X = " + mouseAxis.x + " Y = " + mouseAxis.y);
        lookRotation.SetRotation(mouseAxis);

        //Cursor del ratón
        if (Input.GetMouseButtonDown(0)) mouseCursor.HideCursor();
        else if (Input.GetKeyDown(KeyCode.Escape)) mouseCursor.ShowCursor();

        if (Input.GetMouseButtonDown(0))
        {
            laser.Shot();

        }
        if (Input.GetKeyDown(KeyCode.R)) laser.Reload();

        if(Input.GetMouseButtonDown(1)) ballShoot.Shot();

    }
}