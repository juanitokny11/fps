﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{

    public float maxDistance;
    public LayerMask mask;

    public int maxAmmo;
    public int currentAmmo;
    public float fireRate;
    public float hitForce;
    public float hitDamage;

    public bool isShooting;
    public bool isReloading;
    public float reloadTime;

    public AudioClip[] fx;
    private AudioSource audioSource;
    public Camera camera;

    //Debug
    private Vector3 impactPosition;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        isShooting = false;
        isReloading = false;
        currentAmmo = maxAmmo;
    }

    public void Shot()
    {
        if(isShooting || isReloading) return;
        if(currentAmmo <= 0) return;

        Debug.Log("Shoot");

        PlaySFX(0, 0.75f, true);

        isShooting = true;
        currentAmmo--;

        Ray ray = camera.ScreenPointToRay(Input.mousePosition);
        Debug.DrawRay(this.transform.position, ray.direction, Color.red, 10);
        RaycastHit hit;

        if(Physics.Raycast(ray, out hit, Mathf.Infinity, mask, QueryTriggerInteraction.Collide))
        {
            if(hit.rigidbody != null)
            {
                hit.rigidbody.AddForce(ray.direction * hitForce, ForceMode.Impulse);
            }

            hit.transform.gameObject.SendMessage("Damage", 1);

            impactPosition = hit.point;

            GameObject impact = new GameObject("Shot Impact");
            impact.transform.position = impactPosition;
            AudioSource audioImpact = impact.AddComponent<AudioSource>();
            audioImpact.clip = fx[1];
            audioImpact.volume = 0.15f;
            audioImpact.Play();
            Destroy(impact, fx[1].length);
        }

        StartCoroutine(WaitFireRate());
    }
    private IEnumerator WaitFireRate()
    {
        Debug.Log("Empieza la corutina");
        yield return new WaitForSeconds(fireRate);
        isShooting = false;
        Debug.Log("Termina la corutina");
    }

    public void Reload()
    {
        if(isReloading) return;
        isReloading = true;

        StartCoroutine(WaitForReload());
    }
    private IEnumerator WaitForReload()
    {
        yield return new WaitForSeconds(reloadTime);

        currentAmmo = maxAmmo;
        isReloading = false;
    }

    void PlaySFX(int clip, float volume, bool randomPitch)
    {
        if(fx.Length < clip || clip < 0)
        {
            Debug.LogError("Clip does not exist: " + clip);
            return;
        }

        audioSource.clip = fx[clip];
        audioSource.volume = volume;

        if(randomPitch)
        {
            audioSource.pitch = Random.Range(0.95f, 1.05f);
        }

        audioSource.Play();
    }

    private void OnDrawGizmos()
    {
        if(isShooting)
        {
            Color color = Color.red;
            color.a = 0.4f;
            Gizmos.color = color;
            Gizmos.DrawSphere(impactPosition, 0.35f);
        }
    }
}